# sam-pipeline-gitlab-aws

Creating a CI-CD deployment pipeline for GitLab with my own Gitlab Runner

## Prerequisites in host machine
1. Docker installed

## Run GitLab Runner in a container
- [Gitlab Source](https://docs.gitlab.com/runner/install/docker.html)
1. I choose Option 2: Use Docker volumes to start the Runner container
    - Create the Docker volume:
        - ```sudo docker volume create gitlab-runner-config```
    - Start GitLab Runner container based on ubuntu
        - ```sudo docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest```

2. Register the Runner
    - ```sudo docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register```
    - Use data from Gitlab.com CI/CD->Runners->Expand and disable Shared Runners

3. For build images and solve this problem: dial tcp: lookup thedockerhost on 192.168.100.1:53: no such host
    - [Update config.toml in your runner](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/6295)
    - Inside your runner container 
        - ```apt-get update```
        - ```apt-get -y install curl```

## Another source
[Docker-in-Docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker)
[Configure your own GitLab Runner](https://dev.to/mattdark/configure-your-own-gitlab-runner-4o34)
[var/run/docker.sock](https://www.educative.io/edpresso/var-run-dockersock)
